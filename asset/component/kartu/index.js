import React, { Component } from 'react'
import { Text, View,StyleSheet,Image,ImageBackground } from 'react-native'

class Home extends Component {
    render() {
        return (
           <View style={styles.body}>
                <View style={styles.wrapper}>
                    <View style={styles.head}>
                        <Text style={styles.text}> Member Card </Text>
                    </View>
                </View>
                <View style={styles.wrapper}>
                        <View style={styles.card}>
                            <ImageBackground source={{
                                uri:'https://static.wikia.nocookie.net/aesthetics/images/1/1c/Blue-image.jpg'
                            }} 
                                style={styles.img}
                                imageStyle={{ borderRadius: 20}}
                            />
                            <View style={styles.wraptitle}>
                                {/* <Image 
                                    source={{
                                        uri:'https://www.pngitem.com/pimgs/m/10-104834_coffee-cup-icon-png-transparent-png.png'
                                    }}
                                    style={{
                                        width:50, height:50, resizeMode:'cover', marginRight:10
                                    }}
                                /> */}
                                <Text style={styles.title}>Caffe Kita</Text>
                               
                            </View>
                            <View style={styles.wraptext}>
                                <Text style={styles.textid}>ARUL MARCH</Text>
                                <Text style={styles.textno}>7651 2109 0505 4548</Text>
                                <Text style={styles.textid}>100 Points Rewarded</Text>
                            </View>
                        </View>
                </View>

                {/* ====== CONTENT ======  */}
                <View style={styles.wrappcontent}>
                    <View style={styles.wrapper}>
                        <View style={styles.content}>
                        <Image 
                            source={{
                                uri:'https://cdn0.iconfinder.com/data/icons/shopping-essential-icon-set/100/Shopping-Icons-Essential-Set_13-512.png'
                            }}
                            style={styles.imgcontent}
                        />
                            <Text style={styles.text2}> Hot Promo</Text>
                        </View>

                        <View style={styles.content}>
                        <Image 
                            source={{
                                uri:'https://cdn2.iconfinder.com/data/icons/postal-service-line-to-your-front-door/512/Coupon-512.png'
                            }}
                            style={styles.imgcontent}
                        />
                            <Text style={styles.text2}> Voucher</Text>
                        </View>
                    </View>
                </View>

                <View style={styles.wrappcontent}>
                    <View style={styles.wrapper}>
                        <View style={styles.content}>
                        <Image 
                            source={{
                                uri:'https://static.thenounproject.com/png/14236-200.png'
                            }}
                            style={styles.imgcontent}
                        />
                            <Text style={styles.text2}> Store Location</Text>
                        </View>
                        <View style={styles.content}>
                        <Image 
                            source={{
                                uri:'https://cdn.onlinewebfonts.com/svg/img_568375.png'
                            }}
                            style={styles.imgcontent}
                        />
                            <Text style={styles.text2}> Shop Online</Text>
                        </View>
                    </View>
                </View>
           </View>
        )
    }
}

export default Home

const styles = StyleSheet.create({
    body:{
        backgroundColor:'black',
        flex:1,
    },
    wrapper:{
        flexDirection:'row',
        marginBottom:15
    },
    head:{
        backgroundColor:'blue',
        flex:1,
        height:35,
        alignItems:'center',
        paddingTop:5

    },
    text:{
        fontSize:20,
        color:'#fff',
        fontWeight:'bold'
    },
    // ===== CARD ====== 
    card:{
        marginRight:15,
        marginLeft:15,
        marginTop:20,
        height:200,
        flex:1,
        borderRadius:20,
    },
    img:{
        flex:1,
        height:200,
        borderRadius:20,
    },
    wraptitle:{
       flexDirection:'row',
       justifyContent:'flex-end',
       top:-60
    },
    wraptext:{
        flexDirection:'column'
    },
    title:{
        paddingRight:20,
        color:'black',
        fontSize:22,
        textAlign:'right',
        fontWeight:'bold'
    },
    textid:{
        paddingLeft:20,
        paddingBottom:10,
        color:'white',
        fontSize:18,
    },
    textno:{
        paddingLeft:20,
        paddingBottom:5,
        color:'white',
        fontSize:20,
    },
    

    // ===== CONTENT ===== 
    wrappcontent:{
       flexDirection:'column',
       justifyContent:'space-between',
       marginRight:10,
       marginLeft:10,
       marginTop:5,

    },

    text2:{
        fontSize:15,
        color:'#000',
        marginBottom:10
    },
    content:{
        backgroundColor:'white',
        flex:1,
        height:160,
        marginRight:5,
        marginLeft:5,
        borderRadius:10,
        borderColor:'blue',
        borderWidth:3,
        alignItems:'center'
    },

    imgcontent:{
        flex:1,
        width:80,
        resizeMode: 'contain'
    }

})